package methodReference;

interface Sayable {
    void say();
}

public class withStaticMethod {
    public static void saySomething() {
        System.out.println("Hello, this is static method");
    }
    public static void main(String[] args) {

        // Referring static method
        Sayable sayable = withStaticMethod::saySomething;

        // Calling interface method
        sayable.say();
    }
}
