package lambda;

interface Speakable {
    public String say(String name);
}

public class withSingleParam {
    public static void main(String[] args) {

        // Lambda expression with single parameter
        Speakable sp = (name) -> {
            return "Hello " + name;
        };
        System.out.println(sp.say("World"));


        // We can omit function parentheses
        Speakable sp2 = name -> {
            return "Hello " + name;
        };
        System.out.println(sp2.say("Alien"));
    }
}
