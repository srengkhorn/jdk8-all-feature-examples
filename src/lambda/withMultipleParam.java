package lambda;

interface Addable {
    int add(int a, int b);
}

public class withMultipleParam {
    public static void main(String[] args) {

        // Multiple parameters in lambda expression
        Addable ad = (a, b) -> (a + b);
        System.out.println(ad.add(1,2));

        // Multiple parameters with data type in lambda expression
        Addable ad2 = (int a, int b) -> (a + b);
        System.out.println(ad2.add(2, 2));
    }
}
