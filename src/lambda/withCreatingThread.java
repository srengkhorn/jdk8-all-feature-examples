package lambda;

public class withCreatingThread {

    public static void main(String[] args) {

        // Thread without lambda
        Runnable r1 = new Runnable() {
            @Override
            public void run() {
                System.out.println("Thread 1 is running..");
            }
        };
        Thread thread1 = new Thread(r1);
        thread1.start();

        // Thread example with lambda
        Runnable r2 = () -> {
            System.out.println("Thread 2 is running...");
        };
        Thread thread2 = new Thread(r2);
        thread2.start();

    }
}
