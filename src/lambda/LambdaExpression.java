package lambda;

@FunctionalInterface // It is optional
interface Drawable {
    public void draw();
}

public class LambdaExpression {

    public static void main(String[] args) {
        int width = 11;

        Drawable d = () -> {
          System.out.print("Drawing " + width);
        };
        d.draw();

    }
}
