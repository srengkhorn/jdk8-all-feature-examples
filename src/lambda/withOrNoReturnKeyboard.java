package lambda;

public class withOrNoReturnKeyboard {
    public static void main(String[] args) {

        // Lambda expression without return keyword.
        Addable ad = (a, b)->(a+b);
        System.out.println(ad.add(10, 20));

        // Lambda expression with return keyword.
        Addable ad2 = (int a, int b)->{
            return (a + b);
        };
        System.out.println(ad2.add(20, 20));
    }
}
