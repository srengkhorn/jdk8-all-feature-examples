package lambda;

interface Sayable {
    public String say();
}

public class LambdaNoParam {

    public static void main(String[] args) {

        Sayable s = () -> {
            return "I have nothing to say";
        };
        System.out.print(s.say());
    }
}
