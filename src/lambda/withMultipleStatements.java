package lambda;

interface Saysth{
    String say(String message);
}

public class withMultipleStatements {
    public static void main(String[] args) {

        // We can pass multiple statements in lambda expression
        Saysth person = (message) -> {
            String st1 = "I would like to say, ";
            String st2 = st1 + message;
            return st2;
        };
        System.out.println(person.say("how are you?"));
    }
}
